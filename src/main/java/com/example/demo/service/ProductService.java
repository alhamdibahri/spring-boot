package com.example.demo.service;

import org.springframework.stereotype.Service;

import com.example.demo.model.Product;
import com.example.demo.repository.ProductRepository;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class ProductService {

    private final ProductRepository productRepository;

    public Iterable<Product> getProduct(){
        return productRepository.findAll();
    }

    public Product saveProduct(Product data) {
        return productRepository.save(data);
    }

    public String deleteProduct(Long id) {
        productRepository.deleteById(id);
        return "Deleted Successfully";
    }
}
