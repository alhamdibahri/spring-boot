package com.example.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.model.Product;
import com.example.demo.service.ProductService;

import lombok.RequiredArgsConstructor;

@RestController
@RequiredArgsConstructor
@RequestMapping("api/products")
public class ProductController {

    @Autowired
    private ProductService productService;

    @CrossOrigin(origins = "http://localhost:3000")
    @ResponseBody
    @GetMapping("")
    public ResponseEntity<?> getProducts(){
        Iterable<Product> products = productService.getProduct();
        return new ResponseEntity<>(products, HttpStatus.OK);
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @ResponseBody
    @PostMapping("")
    public ResponseEntity<?> storeProducts(@RequestBody Product product){
        System.out.print("product:" + product.getName());
        Product products = productService.saveProduct(product);
        return new ResponseEntity<>(products, HttpStatus.OK);
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @ResponseBody
    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteProduct(@PathVariable("id") Long id){
        String products = productService.deleteProduct(id);
        return new ResponseEntity<>(products, HttpStatus.OK);
    }
}
